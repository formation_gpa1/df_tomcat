FROM tomcat:latest

ADD jsp-hw.war /usr/local/tomcat/webapps/ROOT.war
ADD server.xml /usr/local/tomcat/conf/server.xml

EXPOSE 8181
VOLUME /usr/local/tomcat/logs
